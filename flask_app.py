
from flask import Flask, request
from datetime import datetime
from datetime import tzinfo
from datetime import timedelta
import numpy as np
from sklearn import linear_model
import telepot
import urllib3
import json
import re
import os

class UTC(tzinfo):
    def utcoffset(self, dt):
        return timedelta(hours=0)
    def dst(self, dt):
        return timedelta(0)
    def tzname(self,dt):
        return "UTC"

proxy_url = "http://proxy.server:3128"
telepot.api._pools = {
    'default': urllib3.ProxyManager(proxy_url=proxy_url, num_pools=3, maxsize=10, retries=False, timeout=30),
}
telepot.api._onetime_pool_spec = (urllib3.ProxyManager, dict(proxy_url=proxy_url, num_pools=1, maxsize=1, retries=False, timeout=30))

secret = ""
bot = telepot.Bot('')
bot.setWebhook("https://Eibriel.pythonanywhere.com/{}".format(secret), max_connections=1)

app = Flask(__name__)

def get_user_info(chat_id):
    filepath = "{}.json".format(chat_id)
    if not os.path.exists(filepath):
        return False
    else:
        with open(filepath, encoding='utf-8') as data_file:
            try:
                data = json.load(data_file)
            except:
                return False
    return data


def save_user_info(chat_id, data):
    filepath = "{}.json".format(chat_id)
    with open(filepath, 'w', encoding='utf-8') as data_file:
        json.dump(data, data_file, sort_keys=True, indent=4, separators=(',', ': '))

def parse_input(text):
    new_reg = re.search(r'(?P<command>(new)|(timezone)|(next))\s+(?P<name>\w+)($|(?P<delta>\s+(-|\+)\s*\d+)|(\s+(?P<date>\d+\/\d+\/\d+\s+)?(?P<time>\d+\:\d+)))', text, re.IGNORECASE)
    new_reg_timezone = re.search(r'(?P<command>timezone)\s+(?P<delta>(-|\+)\s*\d+)', text, re.IGNORECASE)
    if new_reg is None and new_reg_timezone is None:
        if text == "/start":
            return {'command': 'start'}
        elif text == "/help":
            return {'command': 'help'}
        else:
            return {'command': None, 'error-code': 'unknown-command'}
    if new_reg is not None:
        if new_reg.group('command').lower() == "new":
            if new_reg.group('name') is not None:
                if new_reg.group('delta') is not None:
                    try:
                        delta = int(new_reg.group('delta'))
                    except:
                        return {'command': None, 'error-code': 'unknown-delta'}
                    return {'command': 'new', 'subcommand': 'delta', 'name': new_reg.group('name'), 'delta': delta}
                elif new_reg.group('date') is not None and new_reg.group('time') is not None:
                    return {'command': 'new', 'subcommand': 'datetime', 'name': new_reg.group('name'), 'date': new_reg.group('date'), 'time': new_reg.group('time')}
                elif new_reg.group('time') is not None:
                    return {'command': 'new', 'subcommand': 'time', 'name': new_reg.group('name'), 'time': new_reg.group('time')}
                else:
                    return {'command': 'new', 'subcommand': 'nameonly', 'name': new_reg.group('name')}
            else:
                return {'command': 'new', 'error-code': 'new-missing-name'}
        elif new_reg.group('command').lower() == "next":
            return {'command': 'next', 'name': new_reg.group('name')}

    if new_reg_timezone is not None and new_reg_timezone.group('command').lower() == "timezone":
        if new_reg_timezone.group('delta') is not None:
            try:
                delta = int(new_reg_timezone.group('delta'))
            except:
                return {'command': None, 'error-code': 'unknown-delta'}
            return {'command': 'timezone', 'delta': delta}
        else:
            return {'command': None, 'error-code': 'unknown-timezone-delta'}
    return {'command': None, 'error-code': 'unknown-command'}

def add_event(user_info, name, delta=None, date=None, time=None):
    if name not in user_info["events"]:
        user_info["events"][name] = []
    utcc = UTC()
    date_utc = datetime.now(utcc)
    if delta is not None:
        date_utc = date_utc-timedelta(minutes=delta)
    elif date is not None and time is not None:
        try:
            date_utc = datetime.strptime("{} {}".format(date, time), "%d/%m/%y %H:%M")-timedelta(hours=user_info["timezone"])
        except:
            return False
    elif time is not None:
        date_utc = date_utc+timedelta(hours=user_info["timezone"])
        try:
            date_utc = datetime.strptime("{}/{}/{} {}".format(date_utc.day, date_utc.month, date_utc.year, time), "%d/%m/%Y %H:%M")-timedelta(hours=user_info["timezone"])
        except:
            return False
    event = {"datetime": {"day": date_utc.day, "month": date_utc.month, "year": date_utc.year, "hour": date_utc.hour, "minute": date_utc.minute}}
    user_info["events"][name].append(event)
    return True

def get_event(user_info, name):
    if name in user_info["events"]:
        sample_n = 0
        X = []
        Y = []
        for event in user_info["events"][name]:
            X.append([sample_n])
            datetime_tmp = datetime.strptime("{}/{}/{} {}:{}".format(event["datetime"]["day"], event["datetime"]["month"], event["datetime"]["year"], event["datetime"]["hour"], event["datetime"]["minute"]), "%d/%m/%Y %H:%M")
            Y.append(datetime_tmp.timestamp())
            sample_n += 1
        reg = linear_model.LinearRegression()
        reg.fit (X, Y)
        pred = reg.predict([[sample_n]])
        reg.coef_
        datetime_pred = datetime.fromtimestamp(pred[0])+timedelta(hours=user_info["timezone"])
        diff = datetime.fromtimestamp(pred[0])-datetime.utcnow()
        diff_days = diff.days
        diff_hours_td = diff-timedelta(days=diff_days)
        diff_hours = int(diff_hours_td.seconds/3600)
        diff_minutes_td = diff_hours_td-timedelta(hours=diff_hours)
        diff_minutes = int(diff_minutes_td.seconds/60)
        return "X: {}\nY: {}\n\nPrediction: {}\n\nIn {} days, {} hours, {} minutes".format(X, Y, datetime_pred.strftime("%d/%m/%y %H:%M"), diff_days, diff_hours, diff_minutes)
    else:
        return False

@app.route('/{}'.format(secret), methods=["POST"])
def telegram_webhook():
    update = request.get_json()
    text = None
    if "message" in update:
        if "text" in update["message"]:
            text = update["message"]["text"]
            chat_id = update["message"]["chat"]["id"]
    if text is None:
        return "OK NO DATA"

    # Process
    parsed = parse_input(text)

    if "error-code" in parsed:
        bot.sendMessage(chat_id, parsed["error-code"])

    # Responce
    if "command" not in parsed or parsed["command"] is None:
        bot.sendMessage(chat_id, "I can not understand that, please take a look to /help")
        return "OK"

    user_info = get_user_info(chat_id)
    if user_info is False:
        user_info = {"timezone": None, "events":{}}

    if parsed["command"] == "new":
        if user_info["timezone"] is None:
            bot.sendMessage(chat_id, "Please set the timezone first, take a look to /help")
        elif parsed["subcommand"]=="delta":
            add_event(user_info, parsed["name"], parsed["delta"])
            bot.sendMessage(chat_id, "Added new Event \"{}\"".format(parsed["name"]))
        elif parsed["subcommand"]=="datetime":
            add_event(user_info, parsed["name"], date=parsed["date"], time=parsed["time"])
            bot.sendMessage(chat_id, "Added new Event \"{}\"".format(parsed["name"]))
        elif parsed["subcommand"]=="time":
            add_event(user_info, parsed["name"], time=parsed["time"])
            bot.sendMessage(chat_id, "Added new Event \"{}\"".format(parsed["name"]))
        else:
            add_event(user_info, parsed["name"])
            bot.sendMessage(chat_id, "Added new Event \"{}\"".format(parsed["name"]))


    if parsed["command"] == "next":
        events = get_event(user_info, parsed["name"])
        bot.sendMessage(chat_id, events)

    elif parsed["command"] == "timezone":
        try:
            delta = int(parsed["delta"])
        except:
            delta = None
        if delta is None:
            bot.sendMessage(chat_id, "Error reading the Timezone, take a look to /help")
        else:
            user_info["timezone"] = delta
            utcc = UTC()
            date_utc = datetime.now(utcc)+timedelta(hours=user_info["timezone"])
            date_str = date_utc.strftime("%d/%m/%y %H:%M")
            bot.sendMessage(chat_id, "Timezone set to \"{}\". Your time is {}\nIf your time is wrong try again switching the sign of 'diff' (+ or -).".format(delta, date_str))
    elif parsed["command"] == "start":
        bot.sendMessage(chat_id, "Hi, my name is rDany and I will be your Personal AI, or PAI 🥧\nI can record events of your life that occur regurlarly and generate predictions.")
        bot.sendMessage(chat_id, "Start by taking a look to /help to learn how to setup your Timezone.")
    elif parsed["command"] == "help":
        utcc = UTC()
        date_utc = datetime.now(utcc)
        date_str = date_utc.strftime("%d/%m/%y %H:%M")
        bot.sendMessage(chat_id, "To set your Timezone calculate the difference between your clock and my clock (my clock says: {}) and enter the command 'timezone diff'. Where 'diff' is positive or negative number. For example 'timezone -1'.".format(date_str))
        bot.sendMessage(chat_id, "To enter a new event use the command 'new event_name'. Where 'event_name' is the name identifying that event. For example 'new BackFromWork'.")
        bot.sendMessage(chat_id, "Advanced:\nYou can set an offset in minutes, for example 'new BackFromWork -10' if you arrived 10 minutes ago.\nYou can also set the time using 'new BackFromWork 15:30'.\nOr even the date and time with 'new BackFromWork 30/10/18 15:30'.")
        bot.sendMessage(chat_id, "To predict a new occurence of an event use the command 'next evet_name'.")
    save_user_info(chat_id, user_info)
    return "OK"

